
set(GLM_TEST_ENABLE OFF CACHE BOOL "" FORCE)
set(GLM_INSTALL_ENABLE OFF CACHE BOOL "" FORCE)
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/glm")

set(FMT_TEST OFF)
set(FMT_INSTALL OFF)
set(FMT_DOC OFF)

add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/fmt")

# pollutes project with tests, if set ON
set(JSON_BuildTests OFF)
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/json")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/spdlog")

# STB
add_library(stb INTERFACE)
target_include_directories(stb INTERFACE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/stb>
  $<INSTALL_INTERFACE:stb>
)

