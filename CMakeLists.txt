cmake_minimum_required(VERSION 3.11)
cmake_policy(VERSION 3.11)
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

project(Track)

set(CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/libs")
set(CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/projects")

list(APPEND BUILD_ARCHITECTURES
    x86
    amd64
)

set(DEFAULT_COMPILE_FLAGS "-fno-rtti -fno-exceptions")
if (CMAKE_GENERATOR MATCHES "Visual Studio")
	# msvc is retarded.
	# tried to read their documentation on the flags.. now I feel even more dummer than before reading their documentation.
	# if there was a way to do it simple, msvc team will make it complicated, and add some satanistic rituals in it.
	set(DEFAULT_COMPILE_FLAGS "/GR- /EHsc")
endif()

set(PROJECT_VERSION "0.0.1")
add_definitions(-DAPPLICATION_NAME="${PROJECT_NAME}")
add_definitions(-DAPPLICATION_VERSION="${PROJECT_VERSION}")

#set(CMAKE_CXX_FLAGS "-Wall")
set(CMAKE_CXX_FLAGS_RELEASE "-O2")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DNOMINMAX -D_USE_MATH_DEFINES")

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")

include("${CMAKE_CURRENT_LIST_DIR}/libs/CMakeLists.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/projects/CMakeLists.cmake")
