#ifndef TRACK_BUFFER_H_
#define TRACK_BUFFER_H_

#include <cstdint>

namespace track
{
class Buffer
{
public:
    ~Buffer();

    void allocate(uint64_t size);

    uint64_t size() const;
    uint8_t *data() const;
private:
    uint64_t m_size = 0;
    uint8_t *m_data = nullptr;
};
} // ns track

#endif TRACK_BUFFER_H_