#ifndef TRACK_IMAGE2D_H_
#define TRACK_IMAGE2D_H_

#include <cstdint>
#include "buffer.h"

namespace track
{
class Image2D
{
public:
    ~Image2D();

    void allocate(uint64_t width, uint64_t height, uint64_t pixelSizeBytes);

    template <class CPixelType>
    void allocate(uint64_t width, uint64_t height)
    {
        return allocate(width, height, sizeof(CPixelType));
    }
private:
    Buffer *m_buffer = nullptr;
};
} // ns track

#endif TRACK_IMAGE2D_H_

// Vertexes etc are also accessed through Images. they are just different type.