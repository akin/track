#include <iostream>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

int main(int argc, char **argv)
{
	bool logToFile = false;
	if (logToFile)
	{
		auto fileLogger = spdlog::basic_logger_mt(BUILD_PROJECT_NAME, "log.txt", true);
		spdlog::set_default_logger(fileLogger);
	}

	spdlog::info("Welcome!");
	spdlog::info("Project: {0}@{1}", BUILD_PROJECT_NAME, GIT_HASH);
	spdlog::info("Build system: {0}:{1}@{2}", BUILD_SYSTEM_NAME, BUILD_SYSTEM_VERSION, BUILD_SYSTEM_HOSTNAME);

	/*
	double fps = 0.0;
	window.setTitle(fmt::format("{0} - {1}fps", BUILD_PROJECT_NAME, fps));

	if(!window.init({ 800, 600 }))
	{
		spdlog::error("Failed to init window.");
		assert(false);
		return EXIT_FAILURE;
	}

	spdlog::info("window created.");
	Context context;
	ResourceManager resourcemanager(context);

	if(!bnshadertools::load("resources/shaders.json", context, resourcemanager))
	{
		spdlog::error("Failed to load shaders.");
		assert(false);
		return EXIT_FAILURE;
	}
	*/

	return EXIT_SUCCESS;
}
