#include "buffer.h"

namespace track
{
Buffer::~Buffer()
{
    if(m_data)
    {
        delete[] m_data;
        m_data = nullptr;
        m_size = 0;
    }
}

void Buffer::allocate(uint64_t size)
{
    if(m_data)
    {
        delete[] m_data;
    }
    m_size = size;
    m_data = new uint8_t[m_size];
}

uint64_t Buffer::size() const
{
    return m_size;
}

uint8_t *Buffer::data() const
{
    return m_data;
}
} // ns track